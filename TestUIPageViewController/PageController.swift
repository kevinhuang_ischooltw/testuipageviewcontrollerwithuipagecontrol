//
//  PageController.swift
//  TestUIPageViewController
//
//  Created by Kevin Huang on 2017/2/14.
//  Copyright © 2017年 Kevin Huang. All rights reserved.
//

import UIKit

class PageController: UIViewController {

    @IBOutlet weak var pageCtrl: UIPageControl!
    var count = 7;
    var currentIndex = 0;
    
    var pvCtrl : UIPageViewController!
    
    var ctrls : [UIViewController]!
    
    @IBOutlet weak var vwContent: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pageCtrl.numberOfPages = self.count ;
        setCurrentPageIndex();
        
        preparePageViewController();
        
        prepareViewControllers();
        
        self.vwContent.addSubview(self.pvCtrl.view);
        
        self.pvCtrl.view.frame = CGRect(x: 0, y: 0, width: self.vwContent.frame.width, height: self.vwContent.frame.height)
        
        if let firstCtrl = self.ctrls.first {
            self.pvCtrl?.setViewControllers([firstCtrl], direction: UIPageViewControllerNavigationDirection.forward, animated: true) { (isFinish) in
                print("setViewControllers...")
            }
        }
        
    }
    
    //按下上一步按鈕
    @IBAction func btnBeforeTapped(_ sender: UIButton) {
        self.currentIndex = (self.currentIndex <= 0) ? 0 : self.currentIndex - 1 ;
        
        setCurrentPageIndex();
        setPageContent(index: self.currentIndex, direction: UIPageViewControllerNavigationDirection.reverse);
    }
    
    //按下下一步按鈕
    @IBAction func btnAfterTapped(_ sender: UIButton) {
        let maxIndex = self.count - 1 ;
        self.currentIndex = (self.currentIndex >= maxIndex) ? 0 : self.currentIndex + 1 ;
        
        setCurrentPageIndex();
        setPageContent(index: self.currentIndex, direction: UIPageViewControllerNavigationDirection.forward);
    }
    
    //準備 Page View Controller
    private func preparePageViewController() {
        
        self.pvCtrl = UIPageViewController(transitionStyle: UIPageViewControllerTransitionStyle.scroll, navigationOrientation: UIPageViewControllerNavigationOrientation.horizontal, options: nil)
    }
    
    //指定 PageViewController 要顯示的 View
    private func setPageContent(index:Int, direction: UIPageViewControllerNavigationDirection) {
        
        var targetIndex = index;
        if targetIndex < 0 {
            targetIndex = 0;
        }
        
        if targetIndex > self.ctrls.count - 1 {
            targetIndex = self.ctrls.count - 1
        }
        
        let targetVC = self.ctrls[targetIndex]
        
        self.pvCtrl?.setViewControllers([targetVC], direction: direction, animated: true) { (isFinish) in
            print("setViewControllers...")
        }
        
    }
    
    //指定目前要顯示的 PageControl index
    private func setCurrentPageIndex() {
        self.pageCtrl.currentPage = self.currentIndex ;
    }

    //準備要被顯示的所有 View Controllers
    private func prepareViewControllers() {
        // Do any additional setup after loading the view.
        self.ctrls = [self.makeCtrl(color: "red"), self.makeCtrl(color: "green"), self.makeCtrl(color: "blue"), self.makeCtrl(color: "red")]
    }
    //從 Storyboard 建立 View Controller
    private func makeCtrl(color ctrlName:String) -> UIViewController {
        let storyBoard : UIStoryboard = UIStoryboard.init(name: "Main", bundle:nil);
        let ctrl = storyBoard.instantiateViewController(withIdentifier: "\(ctrlName)VC");
        return ctrl
    }
}
