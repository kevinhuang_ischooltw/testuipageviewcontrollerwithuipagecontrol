//
//  PageViewController.swift
//  TestUIPageViewController
//
//  Created by Kevin Huang on 2017/2/14.
//  Copyright © 2017年 Kevin Huang. All rights reserved.
//

import UIKit

class PageViewController: UIPageViewController {
    
    var ctrls : [UIViewController]!
    var theCurrentIndex = 0;

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.ctrls = [self.makeCtrl(color: "red"), self.makeCtrl(color: "green"), self.makeCtrl(color: "blue"), self.makeCtrl(color: "red")]
        
        //self.dataSource = self ;
        
        if let firstCtrl = self.ctrls.first {
            self.setViewControllers([firstCtrl], direction: UIPageViewControllerNavigationDirection.forward, animated: true) { (isFinish) in
                print("setViewControllers...")
            }
        }
        
        
    }
    
    private func makeCtrl(color ctrlName:String) -> UIViewController {
        let storyBoard : UIStoryboard = UIStoryboard.init(name: "Main", bundle:nil);
        let ctrl = storyBoard.instantiateViewController(withIdentifier: "\(ctrlName)VC");
        return ctrl
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PageViewController : UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        var targetIndex = 0
        if let currentIndex = self.ctrls.index(of: viewController) {
            targetIndex = (currentIndex < 1 ? 0 : currentIndex - 1)
        }
        theCurrentIndex = targetIndex ;
        return self.ctrls[targetIndex];
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let maxIndex = (self.ctrls.count == 0 ? 0 : self.ctrls.count - 1 )
        var targetIndex = maxIndex
        if let currentIndex = self.ctrls.index(of: viewController) {
            targetIndex = (currentIndex >= maxIndex ? maxIndex : currentIndex + 1)
        }
        theCurrentIndex = targetIndex ;
        return self.ctrls[targetIndex];
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return self.ctrls.count
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        
        return self.theCurrentIndex;
    }
}

